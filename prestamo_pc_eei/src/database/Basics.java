/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

/**
 *
 * @author eryalus
 */
public class Basics {

    private static final String NOMBRE_DB = "prestamos_eei";
    private static final String IP = "localhost";
    private final String USERNAME, PASSWORD;
    private String port = "";

    public Basics(String user, String pass) {
        USERNAME = user;
        PASSWORD = pass;
    }

    public Basics(String user, String pass, String port) {
        USERNAME = user;
        PASSWORD = pass;
        this.port = port;
    }

    /**
     * Trata de crear la base de datos en caso de no estar creada.
     *
     * @param usuario Usuario de la base de datos
     * @param pass Contraseña de a base de datos
     * @param port Puerto de acceso a la base de datos
     * @return true en caso de terminar la ejecución dejando la base de datos
     * creada o false en caso de haber algún error
     */
    public static boolean crearBaseDatos(String usuario, String pass, String port) {
        try {
            Connection conn = MySQLConnection(usuario, pass, port, "");
            importSQL(conn, new FileInputStream("crear_db.sql"));
        } catch (FileNotFoundException | ClassNotFoundException | SQLException | InstantiationException | IllegalAccessException ex) {
            ex.printStackTrace();
            return false;
        }

        return true;
    }


    protected Connection getConnection() throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        return conectarBaseDatos(USERNAME, PASSWORD, port);
    }

    public static void importSQL(Connection conn, InputStream in) throws SQLException {
        Scanner s = new Scanner(in);
        s.useDelimiter("(;(\r)?\n)|(--\n)");
        Statement st = null;
        try {
            st = conn.createStatement();
            while (s.hasNext()) {
                String line = s.next();
                if (line.startsWith("/*!") && line.endsWith("*/")) {
                    int i = line.indexOf(' ');
                    line = line.substring(i + 1, line.length() - " */".length());
                }

                if (line.trim().length() > 0) {
                    st.execute(line);
                }
            }
        } finally {
            if (st != null) {
                st.close();
            }
        }
    }

    /**
     * Crea una conexión con la base de datos a nivel de la DB del programa
     *
     * @return Conexión con la base de datos ex1.printStackTrace();
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public static Connection conectarBaseDatos(String usuario, String pass, String port) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        return MySQLConnection(usuario, pass, port, NOMBRE_DB);
    }

    /**
     * Crea una conexión con la base de datos dada.
     *
     * @param user usuario
     * @param pass contraseña
     * @param db_name nombre de la base de datos a conectar
     * @return Conexión
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    private static Connection MySQLConnection(String user, String pass, String port, String db_name) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        Connection conn = null;
        if (port.equals("")) {
            conn = DriverManager.getConnection("jdbc:mysql://" + IP + "/" + db_name + "?connectTimeout=15000", user, pass);
        } else {
            conn = DriverManager.getConnection("jdbc:mysql://" + IP + ":" + port + "/" + db_name + "?connectTimeout=15000", user, pass);
        }
        return conn;
    }
}
