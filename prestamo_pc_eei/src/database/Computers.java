/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import data.Computer;
import static database.Basics.conectarBaseDatos;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author eryalus
 */
public class Computers extends Basics {

    public Computers(String user, String pass) {
        super(user, pass);
    }

    public Computers(String user, String pass, String port) {
        super(user, pass, port);
    }

    public boolean addComputer(String marca, String model, Long code) {
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            st.execute("INSERT INTO ordenador(modelo,marca,codigo) values ('" + model + "','" + marca + "'," + code + ")");
            st.close();
            conn.close();
            return true;
        } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(Basics.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public ArrayList<Computer> searchAllFreeComputer() {
        ArrayList<Computer> al = new ArrayList<>();
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet result;
            result = st.executeQuery("select * from ordenador where ordenador.idordenador not in (select ordenador.idordenador from ordenador join prestamo on ordenador.idordenador=prestamo.idordenador)");
            while (result.next()) {
                Computer comp = new Computer();
                comp.setID(result.getInt("idordenador"));
                comp.setCode(result.getLong("codigo"));
                comp.setMarca(result.getString("marca"));
                comp.setModel(result.getString("modelo"));
                comp.setObser(result.getString("observaciones"));
                al.add(comp);
            }
            st.close();
            conn.close();
        } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(Basics.class.getName()).log(Level.SEVERE, null, ex);
        }
        return al;
    }

    public ArrayList<Computer> searchComputer(String marca, String model, String code) {
        ArrayList<Computer> al = new ArrayList<>();
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet result;
            String query = "select * from ordenador ";
            String likes = "";
            if (!marca.equals("")) {
                likes += "marca like '%" + marca + "%'";
            }
            if (!model.equals("")) {
                if (!likes.equals("")) {
                    likes += " and ";
                }
                likes += "modelo like '%" + model + "%'";
            }
            if (!code.equals("")) {
                if (!likes.equals("")) {
                    likes += " and ";
                }
                likes += " CAST(codigo AS char(45)) like '%" + code + "%'";
            }
            if (!likes.equals("")) {
                query += " where " + likes;
            }
            result = st.executeQuery(query);
            while (result.next()) {
                Computer comp = new Computer();
                comp.setID(result.getInt("idordenador"));
                comp.setMarca(result.getString("marca"));
                comp.setModel(result.getString("modelo"));
                comp.setCode(result.getLong("codigo"));
                comp.setObser(result.getString("observaciones"));
                al.add(comp);
            }
            st.close();
            conn.close();
        } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(Basics.class.getName()).log(Level.SEVERE, null, ex);

        }
        return al;
    }
}
