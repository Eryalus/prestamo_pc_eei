/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import data.Person;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author eryalus
 */
public class People extends Basics {

    public People(String user, String pass) {
        super(user, pass);
    }

    public People(String user, String pass, String port) {
        super(user, pass, port);
    }

    public boolean remove(Integer id) {
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            st.execute("delete from prestamo where idpersona="+id);
            st.execute("delete from persona where idpersona=" + id);
            st.close();
            conn.close();
            return true;
        } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(People.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    /**
     *
     * @param id
     * @param p
     * @return 0 ok. -1 exception. -2 duplicated DNI. -3 duplicated NIU
     */
    public int update(Integer id, Person p) {
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            if (p.getDNI() != null) {
                ResultSet result = st.executeQuery("SELECT * FROM persona where DNI='" + p.getDNI() + "' and idpersona!=" + id);
                if (result.next()) {
                    return -2;
                }
            }
            if (p.getNIU() != null) {
                ResultSet result = st.executeQuery("SELECT * FROM persona where NIU='" + p.getNIU() + "' and idpersona!=" + id);
                if (result.next()) {
                    return -3;
                }
            }
            st.execute("update persona set nombre='" + p.getName() + "',apellidos='" + p.getSurnames() + "',DNI='" + p.getDNI() + "',NIU='" + p.getNIU() + "',penalizacion='" + p.getPen() + "'  where idpersona=" + id);
            st.close();
            conn.close();
            return 0;
        } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(People.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;

    }

    public Person getPerson(Integer id) {
        Person p = null;
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet result = st.executeQuery("select * from persona where idpersona=" + id);
            if (result.next()) {
                p = new Person();
                p.setID(id);
                p.setDNI(result.getString("DNI"));
                p.setNIU(result.getString("NIU"));
                p.setName(result.getString("nombre"));
                p.setSurnames(result.getString("apellidos"));
                p.setPen(result.getString("penalizacion"));
            }
            st.close();
            conn.close();
        } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(People.class.getName()).log(Level.SEVERE, null, ex);
        }
        return p;
    }

    /**
     *
     * @param person
     * @return 0 ok. -1 exception. -2 duplicated DNI. -3 duplicated NIU
     */
    public int addPerson(Person person) {
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            if (person.getDNI() != null) {
                ResultSet result = st.executeQuery("SELECT * FROM persona where DNI='" + person.getDNI() + "'");
                if (result.next()) {
                    return -2;
                }
            }
            if (person.getNIU() != null) {
                ResultSet result = st.executeQuery("SELECT * FROM persona where NIU='" + person.getNIU() + "'");
                if (result.next()) {
                    return -3;
                }
            }
            st.execute("INSERT INTO persona(nombre,apellidos,DNI,NIU,penalizacion) values ('" + person.getName() + "','" + person.getSurnames() + "','" + person.getDNI() + "','" + person.getNIU() + "','" + person.getPen() + "')");
            st.close();
            conn.close();
            return 0;
        } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(Basics.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }

    public ArrayList<Person> searchPerson(Person person) {
        ArrayList<Person> al = new ArrayList<>();
        try {
            Connection conn = getConnection();
            Statement st = conn.createStatement();
            ResultSet result;
            String query = "select * from persona ";
            String likes = "";
            if (person.getName() != null) {
                likes += "nombre like '%" + person.getName() + "%'";
            }
            if (person.getSurnames() != null) {
                if (!likes.equals("")) {
                    likes += " and ";
                }
                likes += "apellidos like '%" + person.getSurnames() + "%'";
            }
            if (person.getDNI() != null) {
                if (!likes.equals("")) {
                    likes += " and ";
                }
                likes += "DNI like '%" + person.getDNI() + "%'";
            }
            if (person.getNIU() != null) {
                if (!likes.equals("")) {
                    likes += " and ";
                }
                likes += "NIU like '%" + person.getNIU() + "%'";
            }

            if (!likes.equals("")) {
                query += " where " + likes;
            }
            result = st.executeQuery(query);
            while (result.next()) {
                Person p = new Person();
                p.setDNI(result.getString("DNI"));
                p.setNIU(result.getString("NIU"));
                p.setID(result.getInt("idpersona"));
                p.setName(result.getString("nombre"));
                p.setSurnames(result.getString("apellidos"));
                al.add(p);
            }
            st.close();
            conn.close();
        } catch (SQLException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(Basics.class.getName()).log(Level.SEVERE, null, ex);

        }
        return al;
    }

}
