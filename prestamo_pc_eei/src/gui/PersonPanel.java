/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import data.Person;
import database.People;
import java.util.Date;
import javax.swing.JOptionPane;
import utils.time.MyOwnCalendar;

/**
 *
 * @author eryalus
 */
public class PersonPanel extends javax.swing.JFrame {
    
    private final Person p;
    private final PeopleTable PARENT;

    /**
     * Creates new form PersonPanel
     *
     * @param id
     */
    public PersonPanel(Integer id, PeopleTable parent) {
        initComponents();
        PARENT = parent;
        p = new People(PanelPrincipal.getUsername(), PanelPrincipal.getPassword()).getPerson(id);
        load();
        
    }
    
    private void load() {
        if (p == null) {
            JOptionPane.showMessageDialog(this, "No se han podido cargar los datos de esta persona", "ERROR", JOptionPane.ERROR_MESSAGE);
            return;
        }
        jTextField1.setText(p.getName());
        jTextField2.setText(p.getSurnames());
        if (PanelPrincipal.isValid(p.getDNI())) {
            jTextField3.setText(p.getDNI());
        } else {
            jTextField3.setText("");
        }
        if (PanelPrincipal.isValid(p.getNIU())) {
            jTextField4.setText(p.getNIU());
        } else {
            jTextField4.setText("");
        }
        if (PanelPrincipal.isValid(p.getPen())) {
                MyOwnCalendar cal = new MyOwnCalendar();
                cal.reverseGetDate("[" + p.getPen() + "]", MyOwnCalendar.SPANISH);
                Date d = new Date();
                d.setTime(cal.getTimeInMillis());
                jXDatePicker1.setDate(d);
            
        } else {
            jXDatePicker1.setDate(null);
        }
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jTextField4 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jXDatePicker1 = new org.jdesktop.swingx.JXDatePicker();
        jButton3 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("Nombre:");

        jLabel2.setText("Apellidos:");

        jLabel3.setText("DNI:");

        jLabel4.setText("NIU:");

        jButton1.setText("Guardar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel6.setText("Penalización:");

        jButton2.setText("Restaurar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Borrar");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextField1)
                    .addComponent(jTextField2)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField4)
                            .addComponent(jLabel4)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel6)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jXDatePicker1, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton1)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jXDatePicker1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2)
                    .addComponent(jButton1)
                    .addComponent(jButton3))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        load();        // TODO add your handling code here:
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        save();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        delete();
    }//GEN-LAST:event_jButton3ActionPerformed
    
    private void delete() {
        if (JOptionPane.showConfirmDialog(this, "Se van a borrar todos los datos de esta persona junto con todos sus préstamos pasados y en curso. Está de acuerdo?", "", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            if (new People(PanelPrincipal.getUsername(), PanelPrincipal.getPassword()).remove(p.getID())) {
                JOptionPane.showMessageDialog(this, "Eliminado con éxito");
                PARENT.update();
                this.dispose();
            } else {
                JOptionPane.showMessageDialog(this, "No se ha podido eliminar", "ERROR", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    private void save() {
        Person pers = new Person();
        if (jTextField1.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Debe introducir nombre", "ERROR", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (jTextField2.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Debe introducir apellidos", "ERROR", JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (jTextField3.getText().equals("") && jTextField4.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Debe introducir DNI o NIU", "ERROR", JOptionPane.ERROR_MESSAGE);
            return;
        }
        pers.setName(jTextField1.getText());
        pers.setSurnames(jTextField2.getText());
        if (!jTextField3.getText().equals("")) {
            pers.setDNI(jTextField3.getText());
        }
        if (!jTextField4.getText().equals("")) {
            pers.setNIU(jTextField4.getText());
        }
        if (jXDatePicker1.getDate() != null) {
            pers.setPen(new MyOwnCalendar(jXDatePicker1.getDate().getTime()).getDate(MyOwnCalendar.SPANISH).replace("[", "").replace("]", ""));
        } else {
            pers.setPen(null);
        }
        int resp = new People(PanelPrincipal.getUsername(), PanelPrincipal.getPassword()).update(p.getID(), pers);
        switch (resp) {
            case 0:
                JOptionPane.showMessageDialog(this, "Editado correctamente");
                PARENT.update();
                this.dispose();
                break;
            case -1:
                JOptionPane.showMessageDialog(this, "No se ha podido editar", "ERROR", JOptionPane.ERROR_MESSAGE);
                break;
            case -2:
                JOptionPane.showMessageDialog(this, "Ya hay otra persona con ese DNI", "ERROR", JOptionPane.ERROR_MESSAGE);
                break;
            default:
                JOptionPane.showMessageDialog(this, "Ya hay otra persona con ese NIU", "ERROR", JOptionPane.ERROR_MESSAGE);
                break;
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private org.jdesktop.swingx.JXDatePicker jXDatePicker1;
    // End of variables declaration//GEN-END:variables
}
